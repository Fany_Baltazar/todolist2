//Declara e inicializa array con encabezado
var todo = [''];
var vId = 0;

function AddTask()
{
  //Agrega el dato al array
  var dato = document.getElementById("tarea").value
  todo.push(dato)
  document.getElementById("tarea").value = ""
  Show()
}

function Editar()
{
  var dato = document.getElementById("tarea").value
  todo[vId] = dato
  document.getElementById("tarea").value = ""
  Show()
}

function Show()
{
    document.getElementById("listado").innerHTML = ""

    //recorre el array y muestra lo que se tiene en ToDo.
    for (var index = 0; index < todo.length; index++)
    {
      document.getElementById("listado").innerHTML += '<p id = ' + index + ' onclick="Cargar(this.id)">' + todo[index] + '</p>';
    }
    editar = false
}

function Cargar(id)
{
  document.getElementById("tarea").value = ""
  document.getElementById("tarea").value =  todo[id]
  vId = id
}

//Llamada a la función
//Read()
